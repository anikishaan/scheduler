import React from "react";
import { Row, Col } from "antd";
import Layer from "../component/Layer";
import WeeklySchedule from "../component/weekly/WeeklySchedule";
import TodaySchedule from "../component/daily/TodaySchedule";

export default function SchedulePage() {
  return (
    <Layer>
      <Row gutter={[30, 30]}>
        <Col xs={24} md={24} lg={16}>
          <TodaySchedule />
        </Col>
        <Col xs={24} md={24} lg={8}>
          <WeeklySchedule />
        </Col>
      </Row>
    </Layer>
  );
}

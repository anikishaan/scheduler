import React from "react";
import { Result } from "antd";
import { Link } from "react-router-dom";

export default function NotFound() {
  return (
    <div>
      <Result
        status="404"
        title="404"
        subTitle="Sorry, you are not authorized to access this page."
        extra={<Link to="/">Go to home</Link>}
      />
    </div>
  );
}

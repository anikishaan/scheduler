const WeeklyTask = [
  {
    id: 1,
    title: "Call Doctor for tests",
    time: "15 Mar 2020 - 9:00pm",
    related: ["personal"],
    info: "Ask for blood tests and GYM Certificate",
  },
  {
    id: 2,
    title: "Rifat Birthday",
    time: "09 Sep 2020 - 12:00am",
    related: ["personal", "related"],
    info: "i want to wish for her birthday",
  },
  {
    id: 3,
    title: "Complete the Schedule App",
    time: "28 Sep 2020 - 12:00pm",
    related: ["personal"],
    info:
      "I start to make Scheduler App. 24-sep-2020 and i want to complete this only 4 days",
  },
  {
    id: 4,
    title: "Complete the Schedule App",
    time: "28 Sep 2020 - 12:00pm",
    related: ["personal"],
    info:
      "I start to make Scheduler App. 24-sep-2020 and i want to complete this only 4 days",
  },
  {
    id: 5,
    title: "Complete the Schedule App",
    time: "28 Sep 2020 - 12:00pm",
    related: ["personal"],
    info:
      "I start to make Scheduler App. 24-sep-2020 and i want to complete this only 4 days",
  },
  {
    id: 6,
    title: "Complete the Schedule App",
    time: "28 Sep 2020 - 12:00pm",
    related: ["personal"],
    info:
      "I start to make Scheduler App. 24-sep-2020 and i want to complete this only 4 days",
  },
];

const DailyTask = [
  {
    id: 1,
    title: "Wake Up Time",
    time: "8:00am",
    description: "<p>i want to wake up every day on 8:00am</p>",
  },
  {
    id: 2,
    title: "Daily workout Time",
    time: "10:00am",
    description:
      "<ul><li>Squat 10x3</li><li>Push up 10x3</li><li>Pul Up 5x4</li></ul>",
  },
  {
    id: 3,
    title: "Shift project kick off pt",
    time: "01:00pm",
    description: "<p>i want to wake up every day on 8:00am</p>",
  },
  {
    id: 4,
    title: "Client Meeting",
    time: "05:00pm",
    description:
      "<p>After pray i'll go with client and talk to about her project</p>",
  },
  {
    id: 5,
    title: "Dribbble Shot",
    time: "9:40pm",
    description: "<p>i want to wake up every day on 8:00am</p>",
  },
];

const Related = [
  {
    id: 1,
    name: "personal",
  },
  {
    id: 2,
    name: "client",
  },
  {
    id: 3,
    name: "friends",
  },
  {
    id: 4,
    name: "family",
  },
  {
    id: 5,
    name: "teacher",
  },
  {
    id: 6,
    name: "public",
  },
];

export { WeeklyTask, DailyTask, Related };

import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import {
  Typography,
  Modal,
  Form,
  Input,
  Button,
  DatePicker,
  Select,
} from "antd";
import firebase from "../firebase";
import { Scrollbars } from "react-custom-scrollbars";
import { FileAddOutlined } from "@ant-design/icons";
import CardWeekly from "./CardWeekly";
import "../styles/weeklySchedule.scss";

const { Title } = Typography;
const { Option } = Select;
const { TextArea } = Input;
const db = firebase.firestore();

/**
 * Clock
 */
class Clock extends React.Component {
  constructor(props) {
    super(props);
    this.state = { date: new Date() };
  }

  componentDidMount() {
    this.timerID = setInterval(() => this.tick(), 1000);
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  tick() {
    this.setState({
      date: new Date(),
    });
  }

  render() {
    return this.state.date.toLocaleTimeString();
  }
}
/**
 * Clock End
 */
export default function WeeklySchedule() {
  const [visible, setVisible] = useState(false);
  const [relatedData, setRelatedData] = useState([]);
  const [form] = Form.useForm();

  useEffect(() => {
    db.collection("scheduleUser")
      .doc("anik")
      .collection("Related")
      .onSnapshot((snapshot) => {
        const getRelatedDate = snapshot.docs.map((item) => ({
          id: item.id,
          ...item.data(),
        }));
        setRelatedData(getRelatedDate);
      });
  }, []);

  const onResetForm = () => {
    form.resetFields();
  };
  /**
   * This functions for modal
   */
  const ShowingModal = () => {
    setVisible(true);
  };
  const CloseModal = () => {
    setVisible(false);
  };
  /**
   * This functions for form field
   */
  const onFinish = (values) => {
    console.log("Success:", values);
    db.collection("scheduleUser")
      .doc("anik")
      .collection("WeeklyTask")
      .add({
        title: values.title,
        date: values.date._d,
        description: values.description,
        related: values.related,
      })
      .then(function (docRef) {
        console.log("Document written with ID: ", docRef.id);
      })
      .catch(function (error) {
        console.error("Error adding document: ", error);
      });
    onResetForm();
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <div id="weeklySchedule">
      <Modal
        title="Setup Weekly Reminder"
        visible={visible}
        onOk={CloseModal}
        onCancel={CloseModal}
        footer={null}
      >
        <Form
          form={form}
          name="basic"
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          layout="vertical"
          autoComplete="off"
        >
          <Form.Item
            label="Title"
            name="title"
            rules={[
              {
                required: true,
                message: "Please type any title!",
              },
            ]}
          >
            <Input placeholder="Title" />
          </Form.Item>

          <Form.Item
            label="Pic the Date"
            name="date"
            rules={[
              {
                required: true,
                message: "Please Select the date!",
              },
            ]}
          >
            <DatePicker style={{ width: "100%" }} />
          </Form.Item>
          <Form.Item
            label="Related"
            name="related"
            rules={[
              {
                required: true,
                message: "Please Select the Related!",
              },
            ]}
          >
            <Select
              mode="multiple"
              allowClear
              style={{ width: "100%" }}
              placeholder="Please select"
            >
              {relatedData.map((item) => (
                <Option key={item.name}>{item.name}</Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item
            label="Description"
            name="description"
            rules={[
              {
                required: true,
                message: "Please Select the Description!",
              },
            ]}
          >
            <TextArea rows={3} placeholder="Information" />
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit" onClick={CloseModal}>
              Submit
            </Button>
          </Form.Item>
        </Form>
      </Modal>
      <button className="addWeeklyTask" onClick={ShowingModal}>
        <FileAddOutlined /> Task
      </button>
      <div className="showWeeklyTask">
        <div className="label">
          <Title className="title" level={3}>
            Weekly Pinned
          </Title>
          <Link to="/allWeeklySchedule">View All</Link>
        </div>
        <Scrollbars
          className="showTaskList"
          autoHide
          autoHideDuration={200}
          autoHeight
          autoHeightMin={100}
          autoHeightMax={400}
        >
          <CardWeekly />
        </Scrollbars>
        <div
          className="site-calendar-demo-card"
          style={{ textAlign: "center" }}
        >
          <Title>Clock</Title>
          <Title level={2} style={{ margin: "0", color: "#f8d57d" }}>
            <Clock />
          </Title>
        </div>
      </div>
    </div>
  );
}

import React from "react";
import { Typography, Card, Empty } from "antd";
import ProfileImg from "../ProfileImg";
// import data
import firebase from "../firebase";
const { Title } = Typography;

const db = firebase.firestore();
class CardWeekly extends React.Component {
  state = {
    loading: false,
    weeklyTasks: [],
  };

  /**
   * Firebase Codes
   */
  componentDidMount() {
    db.collection("scheduleUser")
      .doc("anik")
      .collection("WeeklyTask")
      .orderBy("date")
      .limit(5)
      .onSnapshot((snapshot) => {
        const taskItems = snapshot.docs.map((item) => ({
          id: item.id,
          ...item.data(),
        }));
        this.setState({ weeklyTasks: taskItems });
      });
  }
  render() {
    const { loading } = this.state;
    const getWeeklyTask = this.state.weeklyTasks.map((item) => (
      <Card
        className="task-item"
        style={{
          width: "95%",
          margin: "0 auto 16px auto",
          borderRadius: "25px",
        }}
        loading={loading}
        key={item.id}
      >
        <ProfileImg />
        <div className="task-info">
          <Title className="title" level={4}>
            {item.title}
          </Title>
          <time>{new Date(item.date.seconds * 1000).toLocaleDateString()}</time>
          <div className="badge">
            {item.related.map((items, index) => (
              <span key={index}>{items}</span>
            ))}
          </div>
          <p className="small-info" style={{ wordBreak: "break-word" }}>
            {item.description}
          </p>
        </div>
      </Card>
    ));
    return <>{this.state.weeklyTasks.length ? getWeeklyTask : <Empty />}</>;
  }
}

export default CardWeekly;

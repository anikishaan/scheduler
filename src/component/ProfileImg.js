import React, { useState, useEffect } from "react";
import { Avatar } from "antd";
import firebase from "./firebase";

export default function ProfileImg() {
  const db = firebase.firestore();
  const [avatar, settAvatar] = useState("");
  useEffect(() => {
    db.collection("scheduleUser")
      .doc("anik")
      .onSnapshot((doc) => {
        settAvatar(doc.data().avatar);
      });
  });
  return <Avatar src={avatar} />;
}

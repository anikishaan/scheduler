import React from "react";
import { Link } from "react-router-dom";
import Style from "./styles/navbar.module.css";

const MenuItem = ({ url, title }) => {
  return (
    <li>
      <Link to={url}>{title}</Link>
    </li>
  );
};
export default function Navbar() {
  return (
    <nav className={Style.navStyle}>
      <Link className={Style.brand} to="/">
        {process.env.REACT_APP_NAME}
      </Link>
      <ul className={Style.navList}>
        <MenuItem title="Home" url="/" />
        <MenuItem title="All Weekly Schedule" url="/allWeeklySchedule" />
        <MenuItem title="404" url="/notfound" />
      </ul>
    </nav>
  );
}

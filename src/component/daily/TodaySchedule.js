import React, { useState, useEffect } from "react";
import { Typography, Empty } from "antd";
import { FileAddOutlined } from "@ant-design/icons";
import CardToday from "./CardToday";
import firebase from "../firebase";
import "../styles/todaySchedule.scss";
const { Title } = Typography;
const db = firebase.firestore();

export default function TodaySchedule() {
  const [dailyTasks, setDailyTask] = useState([]);
  useEffect(() => {
    db.collection("scheduleUser")
      .doc("anik")
      .collection("DailyTask")
      .onSnapshot((snapshot) => {
        const allDailyTask = snapshot.docs.map((item) => ({
          id: item.id,
          ...item.data(),
        }));
        setDailyTask(allDailyTask);
      });
  }, []);

  const dayName = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
  ];
  const d = new Date();
  const getDailyTask = dailyTasks.map((item) => (
    <CardToday key={item.id} dailyTask={item} />
  ));
  return (
    <div id="todaySchedule">
      <header>
        <div className="upperContent">
          <Title level={2}>Today's Schedule</Title>
          <button className="addTodayTask">
            <FileAddOutlined />
          </button>
        </div>
        <Title level={2} style={{ margin: "0" }} className="txtColor">
          {dayName[d.getDay()]} {d.getUTCDate()}
        </Title>
      </header>
      <div className="todayTaskList">
        {dailyTasks.length ? getDailyTask : <Empty />}
      </div>
    </div>
  );
}

import React, { useState } from "react";
import { Typography } from "antd";
import ProfileImg from "../ProfileImg";
const { Title } = Typography;

export default function CardToday(props) {
  const [showDesc, setShowDesc] = useState(false);
  const showOnClick = () => {
    setShowDesc(!showDesc);
  };
  return (
    <div className="item">
      <header onClick={showOnClick}>
        <div className="profileAndTitle">
          <ProfileImg />
          <Title className="title" level={4}>
            {props.dailyTask.title}
          </Title>
        </div>
        <time>
          {new Date(props.dailyTask.time * 1000).toLocaleTimeString()}
        </time>
      </header>
      <div
        style={showDesc ? { display: "block" } : { display: "none" }}
        className="description"
        dangerouslySetInnerHTML={{ __html: props.dailyTask.description }}
      ></div>
    </div>
  );
}

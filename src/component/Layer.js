import React from "react";

export default function Layer(props) {
  return <main className="mainWrapper">{props.children}</main>;
}

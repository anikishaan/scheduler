import React from "react";
import SchedulePage from "./pages/Schedule";
import AllWeeklySchedule from "./pages/AllWeeklySchedule";
import NotFound from "./pages/404";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import "antd/dist/antd.css";
import "./component/styles/group.scss";
import Navbar from "./component/Navbar";
function App() {
  return (
    <BrowserRouter>
      <Navbar />
      <Switch>
        <Route path="/" exact component={SchedulePage} />
        <Route path="/allWeeklySchedule" component={AllWeeklySchedule} />
        <Route component={NotFound} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
